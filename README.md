"Stand Up Time" - Independent Study on Smart Health - Purdue University, 2015 Spring

This is an Android app prototype designed to prevent people from spending too much time sitting.
It detects if the user is standing or sitting. The user can define an interval to be alerted to stand up. 
